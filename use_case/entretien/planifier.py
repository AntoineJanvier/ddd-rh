from api.charge_de_recrutement import *


def planifier(cr, candidat, creneau):
    cr = get_charge_de_recrutement_available(cr, creneau)
    cr_testable = get_cr_can_test(cr, candidat)
    cr_dispo = get_first_cr_dispo_sooner(cr_testable)
    return generate_entretien(candidat, cr_dispo, creneau)
