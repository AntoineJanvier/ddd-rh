from models.cr import *
from models.candidat import *


def get_random_day(begin, end):
    return random.randrange(begin, end)


def get_ft(d):
    a, b = get_random_day(1, 10), get_random_day(10, 60)
    return d + timedelta(a), d + timedelta(b)


def get_all_cr():
    cr = []
    d = datetime.now()

    b = get_ft(d)
    cr.append(ChargeRecrutement(firstname='Jean', lastname='MICHEL', available_from=b[0], available_to=b[1], can_test_to=1))
    b = get_ft(d)
    cr.append(ChargeRecrutement(firstname='Jackie', lastname='CHAN', available_from=b[0], available_to=b[1], can_test_to=2))
    b = get_ft(d)
    cr.append(ChargeRecrutement(firstname='Jet', lastname='LEE', available_from=b[0], available_to=b[1], can_test_to=3))
    b = get_ft(d)
    cr.append(ChargeRecrutement(firstname='Jean-Jacques', lastname='GOLDMAN', available_from=b[0], available_to=b[1], can_test_to=4))
    b = get_ft(d)
    cr.append(ChargeRecrutement(firstname='Michel', lastname='DUPONT', available_from=b[0], available_to=b[1], can_test_to=5))
    b = get_ft(d)
    cr.append(ChargeRecrutement(firstname='David', lastname='LALA', available_from=b[0], available_to=b[1], can_test_to=6))
    b = get_ft(d)
    cr.append(ChargeRecrutement(firstname='Olivier', lastname='TOM', available_from=b[0], available_to=b[1], can_test_to=7))
    b = get_ft(d)
    cr.append(ChargeRecrutement(firstname='Paul', lastname='AUCHON', available_from=b[0], available_to=b[1], can_test_to=8))
    cr.append(ChargeRecrutement(firstname='Bernard', lastname='TAPIS', available_from=d + timedelta(-60), available_to=d + timedelta(60), can_test_to=20))

    return cr


def get_charge_de_recrutement_available(c, at):
    if not at:
        return None
    r = []
    for cr in c:
        if cr.available_at(at):
            r.append(cr)
    return r


def get_cr_can_test(cr, candidat):
    if not cr or not candidat:
        return None
    r = []
    for c in cr:
        if c.can_test(candidate=candidat):
            r.append(c)
    return r


def get_first_cr_dispo_sooner(cr):
    if not cr:
        return None
    current = cr[0]
    for c in cr:
        if c.available_from < current.available_from:
            current = c
    return current


def generate_entretien(candidat, cr_dispo, date):
    if not candidat or not cr_dispo or not date:
        return None
    e = Entretien(candidate=candidat, date=date, cr=cr_dispo, step='PLANNED')
    candidat.set_entretien_1(date)
    return e
