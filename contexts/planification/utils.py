import random
from datetime import *
from models.candidat import *


def generate_candidates():
    return [
        Candidat(firstname='Antoine', lastname='SAUVINET', techno='.Net'),
        Candidat(firstname='Wael', lastname='ROKBANI', techno='.Net Front / Agilité'),
        Candidat(firstname='Stéphane', lastname='CHAPITEAU', techno='.Net'),
        Candidat(firstname='Guilaume', lastname='NURDIN', techno='.Net Azure'),
        Candidat(firstname='Arnaud', lastname='COUTURIER', techno='.Net'),
        Candidat(firstname='Loic', lastname='THIMON', techno='.Net'),
        Candidat(firstname='Sami', lastname='DAOUES', techno='.Net'),
        Candidat(firstname='Nicolas', lastname='DEVENET', techno='.Net'),
        Candidat(firstname='Geoffroy', lastname='HERB', techno='.Net, Azure'),
        Candidat(firstname='Jonathan', lastname='ABRAHAM', techno='.Net / Front (JS)'),
        Candidat(firstname='Daouda', lastname='BANGOURA', techno='.Net'),
        Candidat(firstname='Emmanuel', lastname='COURMONT', techno='.Net'),
        Candidat(firstname='Christophe', lastname='TRUONG', techno='.Net'),
        Candidat(firstname='Raphael', lastname='WACH', techno='.Net'),
        Candidat(firstname='Romain', lastname='BLATRIX', techno='.Net'),
        Candidat(firstname='Moussa', lastname='FAYE', techno='.Net'),
        Candidat(firstname='Frederic', lastname='NOBRE', techno='Android'),
        Candidat(firstname='Cécile', lastname='HUI BON HOA', techno='IOS'),
        Candidat(firstname='Lamine', lastname='BENDIB', techno='IOS'),
        Candidat(firstname='Guillaume', lastname='GUNTHER', techno='IOS'),
        Candidat(firstname='Maxime', lastname='PAKSERESHT', techno='Front'),
        Candidat(firstname='Lionel', lastname='USUBELLI', techno='Java'),
        Candidat(firstname='Arnaud', lastname='JOLLY', techno='Java Front')
    ]


def generate_entretiens(candidates):
    for candidate in candidates:
        candidate.set_entretien(datetime.now())


def do_codingame():
    return random.choice([True, False])


def rh_choice():
    return random.choice([True, False])


def add_to_vivier(candidate, vivier):
    vivier.append(candidate)
