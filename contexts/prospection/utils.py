from models.candidat import *


def search_candidates():
    return [
        'Adel Mahfoud CHEBBINE',
        'Jawed MOHAMMAD',
        'Julien LAMBY',
        'Olivier LE LAN',
        'Sébastien MENETRIER',
        'Abdoulaye SOW',
        'Yahia FELLAH',
        'Thomas SCANNAGATTI',
        'Jean BAYLON',
        'Valentin MICHALAK',
        'Ludovic PIOT'
    ]


def add_candidate(candidate_list, fname, lname):
    candidate_list.append(Candidat(firstname=fname, lastname=lname))
