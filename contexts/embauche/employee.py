from models.candidat import *


class Employee(Candidat):

    def __init__(self, candidate, salary=None):
        Candidat.__init__(self, firstname=candidate.fname, lastname=candidate.lname, techno=candidate.techno,
                          comment=candidate.comment)
        self.salary = salary

    def set_salary(self, s):
        self.salary = s
