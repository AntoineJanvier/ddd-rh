from datetime import *


class ChargeRecrutement:

    def __init__(self, firstname='EMPTY', lastname='EMPTY', comment='', available_from=datetime.now(),
                 available_to=datetime.now(), can_test_to=1):
        self.fname = firstname
        self.lname = lastname
        self.comment = comment
        self.available_from = available_from
        self.available_to = available_to
        self.can_test_to = can_test_to

    def get_name(self):
        return self.fname + ' ' + self.lname

    def available_at(self, at):
        return self.available_from.timestamp() <= at.timestamp() <= self.available_to.timestamp()

    def can_test(self, candidate):
        return self.can_test_to >= candidate.exp

    def __str__(self):
        return self.get_name()
