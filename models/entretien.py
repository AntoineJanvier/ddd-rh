class Entretien:

    def __str__(self):
        return 'Entretien avec ' + self.cr.get_name()

    def __init__(self, candidate, date, cr=None, step=None):
        self.candidate = candidate
        self.date = date
        self.cr = cr
        self.step = step
