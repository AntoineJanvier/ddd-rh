from contexts.planification.utils import *
from models.entretien import *


class Candidat:

    def __init__(self, firstname='EMPTY', lastname='EMPTY', techno=None, comment='', exp=0):
        self.fname = firstname
        self.lname = lastname
        self.techno = techno
        self.comment = comment
        self.entretien1 = None
        self.entretien2 = None
        self.can_continue = True
        self.exp = exp

    def get_name(self):
        return self.fname + ' ' + self.lname

    def set_entretien_1(self, dt):
        self.entretien1 = Entretien(candidate=self, date=dt, step='PROGRAMMED')

    def set_entretien_2(self, dt):
        self.entretien2 = Entretien(candidate=self, date=dt, step='PROGRAMMED')

    def do_entretien_1(self):
        if self.can_continue is True:
            result = do_codingame()
            if result is False:
                self.can_continue = False
                return False
            self.entretien1.step = 'DONE'
        else:
            return False

    def do_entretien_2(self):
        if self.can_continue is True:
            result = rh_choice()
            if result is False:
                return False
            self.entretien2.step = 'DONE'
        else:
            return False

    def cancel_entretien_1(self):
        self.entretien1 = None

    def cancel_entretien_2(self):
        self.entretien2 = None

    def __str__(self):
        return self.get_name()
