#!/usr/bin/python3
import unittest
from contexts.prospection.utils import *
from use_case.entretien.planifier import *


class TestMethods(unittest.TestCase):

    def test_add_candidate(self):
        candidate_list = []
        l1 = len(candidate_list)

        add_candidate(candidate_list, 'Jean', 'MICHEL')
        l2 = len(candidate_list)

        self.assertGreater(l2, l1, msg='Candidate list must be bigger after adding a candidate')

    def test_set_entretien(self):
        candidate = Candidat(firstname='Jean', lastname='MICHEL')
        entretien1 = candidate.entretien1

        candidate.set_entretien_1(datetime.now())
        entretien2 = candidate.entretien1

        self.assertNotEqual(entretien2, entretien1, msg='Candidate list must be bigger after adding a candidate')

    def test_entretien_1(self):
        candidate = Candidat(firstname='Jean', lastname='MICHEL')
        candidate.set_entretien_1(datetime.now())

        result = candidate.do_entretien_1()

        if result is True:
            self.assertEqual(candidate.entretien1.step, 'DONE', msg='Entretien 1 must be done')

    def test_entretien_2(self):
        candidate = Candidat(firstname='Jean', lastname='MICHEL')
        candidate.set_entretien_2(datetime.now())

        result = candidate.do_entretien_2()

        if result is True:
            self.assertEqual(candidate.entretien2.step, 'DONE', msg='Entretien 2 must be done')

    def test_add_to_vivier(self):
        candidate = Candidat(firstname='Jean', lastname='MICHEL')
        vivier = []

        v1 = len(vivier)
        add_to_vivier(candidate, vivier)
        v2 = len(vivier)

        self.assertGreater(v2, v1, msg='Vivier must grow with the candidate addition')

    def test_can_not_plan_entretien(self):
        candidate = Candidat(firstname='Greg', lastname='PEQUE', exp=30)
        all_cr = get_all_cr()

        entretien = planifier(all_cr, candidate, get_first_cr_dispo_sooner(all_cr).available_from)

        self.assertEqual(entretien, None, msg='Meeting must None')

    def test_can_plan_entretien(self):
        candidate = Candidat(firstname='Greg', lastname='PEQUE', exp=3)
        all_cr = get_all_cr()

        entretien = planifier(all_cr, candidate, get_first_cr_dispo_sooner(all_cr).available_from)

        self.assertNotEqual(entretien, None, msg='Meeting must be created')

    def test_candidate_should_be_able_to_cancel_meeting(self):
        candidate = Candidat(firstname='Greg', lastname='PEQUE', exp=3)
        all_cr = get_all_cr()

        planifier(all_cr, candidate, get_first_cr_dispo_sooner(all_cr).available_from)
        candidate.cancel_entretien_1()

        self.assertEqual(candidate.entretien1, None, msg='Meeting must be created')


if __name__ == '__main__':
    unittest.main()
